import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  changingValue = 15;
  randomValue = Math.random() * 100;
  finishedValue = 100;
}
